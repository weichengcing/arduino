#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>


#define Code "wkBUnd5GH0lRdcUC"
#define SSID "C18C_background"
#define PSK  "qazwsxedc"
#define targetURL  "http://192.168.1.200/"

// 讀取溫度用的腳位
#define ONE_WIRE_BUS 0

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
// 開關的腳位
#define SwitchPin 2
// 設定網頁伺服器監聽80port
ESP8266WebServer server(80);
float temperature=0;
// 開關狀態
bool swichOn=false;
// 計時10分鐘用的
int run_count=100000;
float get_temp(){
    // 要求匯流排上的所有感測器進行溫度轉換
    sensors.requestTemperatures();
    // 取得溫度讀數（攝氏），
    // 參數0代表匯流排上第0個1-Wire裝置
    return sensors.getTempCByIndex(0);
}

void setup() {
    Serial.begin(115200);
    // 設定開關腳位功能為輸出
    pinMode(SwitchPin, OUTPUT);
    digitalWrite(SwitchPin, LOW);
    Serial.println(" start connect ");
    WiFi.mode(WIFI_STA);
    WiFi.begin(SSID, PSK);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(SSID);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    server.on("/switch", []() {
        Serial.print("[HTTP] switch!\n");
        String response="{";
        response+="\"status\":\"ok\",";
        response+="\"temp\":\"";
        response+=get_temp();
        response+="\",";
        response+="\"switchStatus\":";
        Serial.print("switch...");
        Serial.print(server.arg("switch"));
        Serial.print("\n");
        String getswitch=server.arg("switch");
        if(getswitch.equals("1")){
            response+="true";
            swichOn=true;
            digitalWrite(SwitchPin, HIGH);
        }
        else{
            response+="false";
            swichOn=false;
            digitalWrite(SwitchPin, LOW);
        }
        response+="}";
        server.send(200, "text/plain", response);
    });
    server.begin();
    Serial.println("HTTP server started");
}
void loop() {
    // 處理client功能
    server.handleClient();
    if(WiFi.status() == WL_CONNECTED){
        run_count+=1;
        // Serial.print("count...");
        // Serial.print(run_count);
        // Serial.print("\n");
        // 10分鐘傳送一次溫度跟開關狀態
        if(run_count>30000){
            run_count=0;
            WiFiClient client;
            HTTPClient http;
            String url=targetURL;
            url+="index/sync/?temp=";
            url+=get_temp();
            url+="&switch_status=";
            if(swichOn){
                url+="1";
            }
            else{
                url+="0";
            }
            url+="&code=";
            url+=Code;
            if (http.begin(client, url)) {  // http
                // start connection and send HTTP header
                int httpCode = http.GET();

                // httpCode will be negative on error
                if (httpCode > 0) {
                    // HTTP header has been send and Server response header has been handled
                    Serial.printf("[HTTP] GET... code: %d\n", httpCode);

                    // file found at server
                    if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
                        String payload = http.getString();
                        Serial.println(payload);
                    }
                } else {
                    Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
                }
                http.end();
            }
        }
    }
    delay(10);
}