#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <Servo.h>

Servo servo_h;
Servo servo_m;
#define SSID "C18C"
#define PSK "0426983426"
#define ledPin 2
unsigned int localPort = 2390;               //local port to listen for UDP packets
IPAddress timeServerIP;                      //time.nist.gov NTP server address
const char *ntpServerName = "time.nist.gov"; //NTP Server host name
const int NTP_PACKET_SIZE = 48;              // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[NTP_PACKET_SIZE];          //buffer to hold incoming and outgoing packets
WiFiUDP udp;                                 //UDP instance to let us send and receive packets over UDP
unsigned long t = 2208988800UL;

boolean updatetime = true;
const unsigned long onehour = 3600UL;
void setup()
{
    //Serial.begin(115200);
    //Serial.println("初始化馬達");
    servo_h.attach(0);
    servo_m.attach(3);
    servo_h.write(0);
    servo_m.write(0);
    servo_m.write(180);
    servo_h.write(180);
    delay(1000);
    servo_m.write(0);
    servo_h.write(0);
    pinMode(ledPin, OUTPUT);
    // wifi連線
    //Serial.println(" start connect ");
    WiFi.begin(SSID, PSK);
    if (WiFi.status() != WL_CONNECTED)
    {
        digitalWrite(ledPin, HIGH);
        delay(500);
        //Serial.println(".");
        digitalWrite(ledPin, LOW);
    }
    //Serial.println("WiFi connected");
    //Serial.print("IP address: ");
    //Serial.println(WiFi.localIP());
    //Start UDP
    //Serial.println("Starting UDP");
    udp.begin(localPort);
}
void loop()
{
    // 每分鐘再更新時間
    if (t % 300 < 10)
    {
        updatetime = true;
    }
    if (updatetime)
    {
        //get a random server from the pool (get an IP from Server Name)
        WiFi.hostByName(ntpServerName, timeServerIP);
        sendNTPpacket(timeServerIP); //send an NTP packet to a time server
        delay(1000);                 // wait to see if a reply is available

        int cb = udp.parsePacket(); //return bytes received
        if (!cb)
        {
            digitalWrite(ledPin, HIGH);
            //Serial.print("no packet yet  ");
            //Serial.print("IP address: ");
            //Serial.println(WiFi.localIP());
        }
        else
        {
            //received a packet, read the data from the buffer
            //Serial.print("packet received, length=");
            //Serial.println(cb);                      //=48
            udp.read(packetBuffer, NTP_PACKET_SIZE); //read the packet into the buffer

            //the timestamp starts at byte 40 of the received packet and is four bytes,
            //or two words, long. First, esxtract the two words:
            unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
            unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
            //combine the four bytes (two words) into a long integer
            //this is NTP time (seconds since Jan 1 1900):
            unsigned long secsSince1900 = highWord << 16 | lowWord;
            //Serial.print("Seconds since Jan 1 1900 = ");
            //Serial.println(secsSince1900);

            //now convert NTP time into everyday time:
            //Serial.print("Unix time = ");
            // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
            const unsigned long seventyYears = 2208988800UL;
            // subtract seventy years:
            unsigned long epoch = secsSince1900 - seventyYears;
            // 時區修正
            t = epoch + (onehour * 8);
            // print Unix time:
            //Serial.println(t);
            updatetime = false;
        }
    }
    digitalWrite(ledPin, LOW);
    t++;
    delay(500);
    digitalWrite(ledPin, HIGH);
    delay(500);
    digitalWrite(ledPin, LOW);
    //Serial.println("現在時間(GMT+8):"); //UTC=Greenwich Meridian (GMT)
    //Serial.print((t % 43200L) / 3600);  //print the hour (86400 secs/day)
    servo_h.write((t % 43200L) / 3600 * 30);
    //Serial.print(" 時 ");
    //In the first 10 minutes of each hour, we'll want a leading '0'
    if ((t % 3600) / 60 < 10)
    {
        //Serial.print("0");
    }
    //Serial.print((t % 3600) / 60);
    servo_m.write(180 - ((t % 3600) / 60 * 3));

    //Serial.print(" 分 "); // print the minute (3600 secs/minute)
    // In the first 10 seconds of each minute, we'll want a leading '0'
    if ((t % 60) < 10)
    {
        //Serial.print('0');
    }
    //Serial.print(t % 60); // print the second
    //Serial.println(" 秒");
}
unsigned long sendNTPpacket(IPAddress &address)
{
    //Serial.print("sending NTP packet...");
    // set all bytes in the buffer to 0
    memset(packetBuffer, 0, NTP_PACKET_SIZE); //clear the buffer
    //Initialize values needed to form NTP request
    //(see URL above for details on the packets)
    packetBuffer[0] = 0b11100011; // LI, Version, Mode
    packetBuffer[1] = 0;          // Stratum, or type of clock
    packetBuffer[2] = 6;          // Polling Interval
    packetBuffer[3] = 0xEC;       // Peer Clock Precision
    //8 bytes of zero for Root Delay & Root Dispersion
    packetBuffer[12] = 49;
    packetBuffer[13] = 0x4E;
    packetBuffer[14] = 49;
    packetBuffer[15] = 52;
    // all NTP fields have been given values, now
    // you can send a packet requesting a timestamp:
    udp.beginPacket(address, 123);            //NTP requests are to port 123
    udp.write(packetBuffer, NTP_PACKET_SIZE); //send UDP request to NTP server
    udp.endPacket();
}