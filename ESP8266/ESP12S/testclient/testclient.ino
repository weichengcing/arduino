#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "eepfunc.h";

// 儲存用
String check_code = "";
char wifi_ssid_private[32];
char wifi_password_private[32];

#ifndef STASSID
#define STASSID "C18C_background"
#define STAPSK  "qazwsxedc"
#endif
// 讀取溫度用
#define ONE_WIRE_BUS 0
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
const char* ssid = STASSID;
const char* password = STAPSK;
const int switch_pin = 2;
float temperature=0;
int touch_count=0;
ESP8266WebServer server(80);
void setup() {
  pinMode(switch_pin, OUTPUT);
  Serial.begin(115200);
  // 第一步先讀取檢查碼如果檢查碼是000000就是沒有連線
  readEEPROM(0,6,check_code);
  if(check_code=="000000"){
    // 未連線
    Serial.println(" no data! ");
    check_code="000001";
    writeEEPROM(0,6,check_code);
  }
  else{
    Serial.print(" data... ");
    Serial.println(check_code);
  }





  Serial.println(" start connect ");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  server.on("/", []() {
    Serial.print("[HTTP] touch...\n");
    String reply;
    reply=" Now temperature is ";
    reply+=temperature;
    reply+=' C\n';
    server.send(200, "text/plain", reply);
    Serial.print(reply);
  });
  server.on("/writ", []() {
    Serial.print("[HTTP] write...\n");
    check_code="000000";
    writeEEPROM(0,6,check_code);
    server.send(200, "text/plain", "ok");

  });
  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  // 要求匯流排上的所有感測器進行溫度轉換
  sensors.requestTemperatures();
  // 取得溫度讀數（攝氏），
  // 參數0代表匯流排上第0個1-Wire裝置
  temperature=sensors.getTempCByIndex(0);
  server.handleClient();
  touch_count++;
  // wait for WiFi connection
  if (WiFi.status() == WL_CONNECTED && touch_count%1000==0) {
    touch_count=0;
    WiFiClient client;
    HTTPClient http;
    Serial.print("[HTTP] begin...\n");
    String url;
    url="http://192.168.1.110/?A=touch_";
    url+=touch_count;
    if (http.begin(client, url)) {  // http
      // start connection and send HTTP header
      int httpCode = http.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }
      http.end();
    } else {
      Serial.printf("[HTTP} Unable to connect\n");
    }
  }

  delay(10);
}