
#include <FS.h>

void setup(){
    Serial.begin(115200);
    Serial.print("START:");
    // 啟動檔案功能
    SPIFFS.begin();
     // this opens the file "f.txt" in read-mode
    File f = SPIFFS.open("/config.txt", "r");

    if (f) {
        int c=0;
        // we could open the file
        while(f.available()) {
            //Lets read line by line from the file
            String line = f.readStringUntil('n');
            if(c%2==1){
                Serial.println(line);
            }
            c++;
        }

    }
    f.close();

}
void loop(){

}