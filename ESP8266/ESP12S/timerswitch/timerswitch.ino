#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <time.h>                       // time() ctime()
#include <FS.h>

String Code = "wkBUnd5GH0lRdcUC";
String SSID = "";
String PSK = "";

// 開關的腳位
#define SwitchPin 2
// 設定網頁伺服器監聽80port
ESP8266WebServer server(80);
// 開關狀態
bool swichOn=false;
// 計時10分鐘用的
int run_count=100000;
void clear_conf(){

}
void setup() {
    // 設定開關腳位功能為輸出
    pinMode(SwitchPin, OUTPUT);
    digitalWrite(SwitchPin, LOW);
    Serial.begin(115200);
    Serial.println("");
    // 啟動檔案功能
    SPIFFS.begin();
    if(!SPIFFS.exists("/config.txt")){
        // 沒有檔案就變成AP
        IPAddress local_IP(192,168,0,1);
        IPAddress gateway(192,168,0,1);
        IPAddress subnet(255,255,255,0);
        Serial.print("Setting soft-AP configuration ... ");
        Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed!");

        Serial.print("Setting soft-AP ... ");
        Serial.println(WiFi.softAP("wifi_water_heat") ? "Ready" : "Failed!");

        Serial.print("Soft-AP IP address = ");
        Serial.println(WiFi.softAPIP());
        server.on("/", []() {
            File file_index = SPIFFS.open("/index.html", "r");
            server.streamFile(file_index, "text/html");
            file_index.close();
        });
        server.on("/setting", []() {
            File f_conf = SPIFFS.open("/config.txt", "w");
            f_conf.println("SSID");
            String ssid=server.arg("ssid");
            f_conf.println(ssid);
            f_conf.println("psk");
            String psk=server.arg("psk");
            f_conf.println(psk);
            f_conf.println("※");
            for(int i =0;i<24;i++){
                f_conf.println(i);
            }
            f_conf.close();
            server.send(200, "text/plain", "帳密已經儲存將自動重啟");
            ESP.restart();
        });

        server.begin();
        while(true){
            server.handleClient();
            delay(10);
        }
    }
    else{
        File f = SPIFFS.open("/config.txt", "r");
        f.readStringUntil('\n');
        SSID=f.readStringUntil('\n');
        int len=SSID.length();
        SSID=SSID.substring(0,len-1);
        f.readStringUntil('\n');
        PSK=f.readStringUntil('\n');
        len=PSK.length();
        PSK=PSK.substring(0,len-1);
        f.close();
        Serial.println(" start connect ");
        Serial.println(SSID.length());
        Serial.println(SSID);
        Serial.println(PSK);
        WiFi.mode(WIFI_STA);
        WiFi.begin(SSID, PSK);
        while (WiFi.status() != WL_CONNECTED) {
            delay(500);
            Serial.print(".");
        }
        Serial.println("");
        Serial.print("Connected to ");
        Serial.println(SSID);
        Serial.print("IP address: ");
        Serial.println(WiFi.localIP());
        server.on("/", []() {
            File file_index = SPIFFS.open("/show.html", "r");
            server.streamFile(file_index, "text/html");
            file_index.close();
        });
        server.on("/get", []() {
            File f = SPIFFS.open("/config.txt", "r");
            f.readStringUntil('※');
            f.readStringUntil('\n');
            String res="[";
            for(int i =0;i<24;i++){
                res+=f.readStringUntil('\n');
                if(i<23){
                    res+=',';
                }
            }
            res+="]";
            f.close();
            server.send(200, "text/plain", res);
        });
        server.on("/switch", []() {

        });

    }

    server.begin();
    Serial.println("HTTP server started");
}
void loop() {
    // 處理client功能
    server.handleClient();
    if(WiFi.status() == WL_CONNECTED){
    }
    delay(10);
}