#include "DHTesp.h"

#ifdef ESP32
#pragma message(THIS EXAMPLE IS FOR ESP8266 ONLY!)
#error Select ESP8266 board.
#endif

DHTesp dht;

void setup()
{
  Serial.begin(115200);
  // Autodetect is not working reliable, don't use the following line
  // dht.setup(17);
  // use this instead: 
  dht.setup(0, DHTesp::DHT22); 
}

void loop()
{
  delay(dht.getMinimumSamplingPeriod());

  float humidity = dht.getHumidity();
  float temperature = dht.getTemperature();

  Serial.print("相對濕度\t");
  Serial.println(humidity, 1);
  Serial.print("溫度\t");
  Serial.print(temperature, 1);
  Serial.print("露點溫度\t");
  Serial.println(dht.computeDewPoint(temperature, humidity, false));
  Serial.print("舒適度\t");
  Serial.println(dht.computePerception(temperature, humidity, false), 1); 
  delay(2000);
}
