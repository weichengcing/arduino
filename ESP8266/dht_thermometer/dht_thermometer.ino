// 要上傳檔案到esp8266參考
// https://swf.com.tw/?p=905

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include "DHTesp.h"
DHTesp dht;
// String SSID = "ken";
// String PSK = "ken@24433392";
String SSID = "SSID";
String PSK = "PSK";
// 溫度
float temperature = 0;
// 濕度
float humidity = 0;
// 計數器
int count = 0;
// 設定網頁伺服器監聽80port
ESP8266WebServer server(80);
void setup()
{
    Serial.begin(115200);
    Serial.println("");
    // wifi功能
    WiFi.mode(WIFI_STA);
    WiFi.begin(SSID, PSK);
    Serial.print("等待wifi連接");
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.print("Connected to ");
    Serial.println(SSID);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    dht.setup(0, DHTesp::DHT22);
    // 啟動檔案功能
    SPIFFS.begin();
    // 設定server
    server.on("/", []() {
        File file_index = SPIFFS.open("/index.html", "r");
        server.streamFile(file_index, "text/html");
        file_index.close();
    });
    server.on("/data", []() {
        String response = "";
        response += "{";
        response += "\"temp\":\"";
        response += temperature;
        response += "\",";
        response += "\"humidity\":\"";
        response += humidity;
        response += "\",";
        response += "\"check\":\"";
        response += server.arg("check");
        response += "\"}";
        server.send(200, "text/plain", response);
    });
    server.begin();
    Serial.println("HTTP server started");
    Serial.print("getMinimumSamplingPeriod");
    Serial.println(dht.getMinimumSamplingPeriod());
    count = dht.getMinimumSamplingPeriod();
}
void loop()
{
    // 處理client功能
    server.handleClient();

    if (count <= dht.getMinimumSamplingPeriod())
    {
        count++;
    }
    else
    {
        humidity = dht.getHumidity();
        temperature = dht.getTemperature();

        Serial.println("----------------");
        Serial.print("相對濕度\t");
        Serial.println(humidity, 1);
        Serial.print("溫度\t");
        Serial.println(temperature, 1);
        Serial.print("露點溫度\t");
        Serial.println(dht.computeDewPoint(temperature, humidity, false));
        Serial.print("舒適度\t");
        Serial.println(dht.computePerception(temperature, humidity, false), 1);
        count = 0;
    }
    delay(1);
}