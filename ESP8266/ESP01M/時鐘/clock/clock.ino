// Generic ESP8266 Module
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include<Ticker.h>
#include "DateTime.h"
#include "PinOutGroup.h"
Ticker timer;
// wifi設定
const char * ssid = "CCC";
const char * pass = "O426983426";

unsigned int localPort = 2390;
IPAddress timeServerIP;
const char* ntpServerName = "tock.stdtime.gov.tw";
const int NTP_PACKET_SIZE = 48;
byte packetBuffer[ NTP_PACKET_SIZE];
WiFiUDP udp;

DateTime now;
unsigned long devicetime=0;
// 數字顯示掃描
PinOutGroup POG;
PinOutGroup POGscan;
// Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
const unsigned long seventyYears = 2208988800UL;
int num[4]={0,0,0,0};
int update_time_counter=600;
int h,m,s;

void setup() {
  Serial.begin(115200);
  // 初始化輸出port
  POG.add(15);
  POG.add(13);

  POG.add(12);
  POG.add(14);
  POGscan.add(4);
  POGscan.add(0);
  POGscan.add(2);
  POG.write(DecToBcd(0));
  POGscan.write(DecToBcd(0));
  pinMode(16,OUTPUT);
  // wifi連線
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  int wait=0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    POGscan.write(DecToBcd(wait));
    if(wait<4){
      wait+=1;
    }
    else{
      wait=0;
    }
  }
  udp.begin(localPort);
}

void loop() { 
  if(update_time_counter>=100){
    timer.detach();
    update_time_counter=0;
    WiFi.hostByName(ntpServerName, timeServerIP);
    bool gettime=false;
    bool dot=false;
    while(!gettime){ 
      sendNTPpacket(timeServerIP);
      delay(500);
      int cb = udp.parsePacket();
      if(dot){
        digitalWrite(16,HIGH);
      }
      else{
        digitalWrite(16,LOW);
      } 
      dot=!dot;
      if (!cb) {
        Serial.println("no packet yet");
      } else {
        gettime=true;
        Serial.print("packet received, length=");
        Serial.println(cb);
        // We've received a packet, read the data from it
        udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

        //the timestamp starts at byte 40 of the received packet and is four bytes,
        // or two words, long. First, esxtract the two words:

        unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
        unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
        // combine the four bytes (two words) into a long integer
        // this is NTP time (seconds since Jan 1 1900):
        unsigned long secsSince1900 = highWord << 16 | lowWord;
        const unsigned long seventyYears = 2208988800UL;
        const unsigned long hour_8=28800UL;
        // subtract seventy years:
        devicetime = secsSince1900 - seventyYears + hour_8+30;
        now=devicetime;
        Serial.print("Adjusted RTC time is: ");
        Serial.println(now.timestamp(DateTime::TIMESTAMP_FULL));
        timer.attach(1,add_one_sec);
      }
    }
  }
  
  for(int s=3;s>=0;s--){
    POG.write(DecToBcd(num[s]));
    POGscan.write(DecToBcd(s));
    delay(4);
  }
}
byte DecToBcd(byte val)
{
   return( ((val/10) << 4) | (val%10) );
}
void sendNTPpacket(IPAddress& address) {
  Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}
// 加1秒
void add_one_sec(){
  devicetime+=1;
  update_time_counter+=1;
  now=devicetime;
  Serial.println(now.timestamp(DateTime::TIMESTAMP_FULL));
  h=now.hour();
  if(h<10){
    num[3]=0;
    num[2]=h;
  }
  else{
    num[3]=floor(h/10);
    num[2]=h%10;
  }

  m=now.minute(); 
  if(m<10){
    num[1]=0;
    num[0]=m;
  }
  else{
    num[1]=floor(m/10);
    num[0]=m%10;
  }

  s=now.second();
  if(s%2==0){
    digitalWrite(16,HIGH);
  }
  else{
    digitalWrite(16,LOW);
  }
}
