#include"TFT_eSPI.h"
TFT_eSPI tft;
#define LCD_BACKLIGHT (72Ul)
int plan_x=100;
int plan_y=50;
int bullet_x[100];
int bullet_y[100];
int bullet_count=0;
bool shut=false;
int target=400;
bool target_live=false;
void setup() {
  // put your setup code here, to run once:
  pinMode(WIO_BUZZER,OUTPUT);
  pinMode(WIO_5S_UP, INPUT_PULLUP);
  pinMode(WIO_5S_DOWN, INPUT_PULLUP);
  pinMode(WIO_5S_LEFT, INPUT_PULLUP);
  pinMode(WIO_5S_RIGHT, INPUT_PULLUP);
  pinMode(WIO_5S_PRESS, INPUT_PULLUP);
  pinMode(WIO_KEY_C, INPUT_PULLUP);
  tft.begin();
  tft.fillScreen(TFT_WHITE);
  digitalWrite(LCD_BACKLIGHT, HIGH);
  tft.setRotation(0);
}

void loop() {
  draw_plan(true);
  draw_bullet(true);
  draw_target(true);
  if (digitalRead(WIO_5S_UP) == LOW) {
    // 往上走
    if(plan_x>0){
      plan_x=plan_x-1;
    }
  }
  else if(digitalRead(WIO_5S_DOWN) == LOW){
    if(plan_x<235){
      plan_x=plan_x+1;
    }
    
  }
  else if(digitalRead(WIO_5S_RIGHT) == LOW){
    if(plan_y>5){
      plan_y=plan_y-1;
    }
  }
  else if(digitalRead(WIO_5S_LEFT) == LOW){
    if(plan_y<317){
      plan_y=plan_y+1;
    }
  }
  // 發射
  if(!shut && digitalRead(WIO_KEY_C)==LOW){
    shut=true;
    bullet_x[bullet_count]=plan_x;
    bullet_y[bullet_count]=plan_y;
    if(bullet_count<99){
      bullet_count=bullet_count+1;
    }
    else{
      bullet_count=0;
    }
    analogWrite(WIO_BUZZER,128);
  }else{
    // 不走
    shut=false;
    analogWrite(WIO_BUZZER,0);
  } 
  for (int i=0; i<100; i++) {
    if(bullet_x[i]>0){
      bullet_x[i]=bullet_x[i]-1;
    }
  }
  if(!target_live){
    target=random(320);
  }
  
  draw_bullet(false);
  draw_target(false);
  draw_plan(false);
  delay(10);
}
void draw_plan(bool clear){
  uint16_t color=TFT_BLACK;
  if(clear){
    color=TFT_WHITE;
  }
  tft.drawTriangle(plan_x, plan_y, plan_x+5, plan_y+3, plan_x+5, plan_y-3, color);
}
void draw_bullet(bool clear){
  uint16_t color=TFT_RED;
  if(clear){
    color=TFT_WHITE;
  }
  for (int i=0; i<100; i++) {
    if(bullet_x[i]>0){
      tft.drawPixel(bullet_x[i],bullet_y[i],color);
    }
  }
}
void draw_target(bool clear){
  uint16_t color=TFT_RED;
  if(clear){
    color=TFT_WHITE;
  }
  if(target_live){
    tft.drawRect(10,target,5,5,color);
  }
}

