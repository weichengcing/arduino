//#include <Thermistor.h>
//Thermistor temp(0);
void setup() {
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  digitalWrite(13, HIGH);
  Serial.begin(9600);
}

// the loop function runs over and over again forever
void loop() {
   Serial.print("A");
  for(int i=0;i<1000;i+=100){
    digitalWrite(13, HIGH);
    delay(i);
    digitalWrite(13, LOW);
    delay(i);
  }
  digitalWrite(12, HIGH);
  delay(1000);
  digitalWrite(12, LOW);
  Serial.print("B");
}
