#include <LiquidCrystal.h>
#include <DS1302.h>
#include "Thermistor.h"
LiquidCrystal lcd(12,11,5,4,3,2);
const int cepin=10;
const int iopin=9;
const int sclkpin=8;
DS1302 rtc(cepin, iopin, sclkpin);
Thermistor temp(0);
void setup() {
  // put your setup code here, to run once:
  lcd.begin(16,2);
  lcd.clear();
  rtc.writeProtect(false);// 是否防止寫入 (日期時間設定成功後即可改成true)
  rtc.halt(false);// 是否停止計時
  //Time t(2017, 4, 2, 13, 15, 00, Time::kSunday); //年 月 日 時 分 秒 星期幾 (日期時間設定成功後即可註解掉)
  //rtc.time(t);
}
boolean count=false;
  int i=0;
void loop() {
  //印出日期START
  lcd.setCursor(0,0);
  Time t = rtc.time();
  lcd.print(t.yr);
  lcd.print("/");
  if(t.mon<10){
    lcd.print("0");
  }
  lcd.print(t.mon);
  lcd.print("/");
  if(t.date<10){
    lcd.print("0");
  }
  lcd.print(t.date);
  //印出日期END
  float temperature =temp.getTemp();
  lcd.setCursor(12 ,0);
  lcd.print(int(temperature));
  lcd.print("*C");
  lcd.setCursor(0,1);
  if(t.hr<10){
    lcd.print("0");
  }
  lcd.print(t.hr);
  lcd.print(":");
  if(t.min<10){
    lcd.print("0");
  }
  lcd.print(t.min);
  lcd.print(":");
  if(t.sec<10){
    lcd.print("0");
  }
  lcd.print(t.sec);
  if(t.sec%2==1){
    lcd.print(" ");
  }
  else{
    lcd.print(" .");
  }
  lcd.print(".");
  delay(1000);
  lcd.clear();
  
}
