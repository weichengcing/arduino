#include <SPI.h>
#include "Thermistor.h"
#include "nRF24L01.h"
#include "RF24.h"
Thermistor temp(0);
//for nrf24 debug
int serial_putc( char c, FILE * ) 
{
  Serial.write( c );
  return c;
} 

//for nrf24 debug
void printf_begin(void)
{
  fdevopen( &serial_putc, 0 );
}

//nRF24 set the pin 9 to CE and 10 to CSN/SS
// Cables are:
//     SS       -> 10
//     MOSI     -> 11
//     MISO     -> 12
//     SCK      -> 13

	RF24 radio(9,10);

	//we only need a write pipe, but am planning to use it later
	const uint64_t pipes[2] = { 0xF0F0F0F0E1LL,0xF0F0F0F0D2LL };
	// here we can send up to 30 chars
	char SendPayload[31] = "blog.riyas.org";
	/*宣告自己的名稱*/
	char myname[]="temp1-------------------------";
void setup(void) {
	Serial.begin(57600); //Debug 
	printf_begin();
	//nRF24 configurations
	radio.begin();
	radio.setChannel(0x60);
	radio.setAutoAck(1);
	radio.setRetries(15,15);
	radio.setDataRate(RF24_250KBPS);
	radio.setPayloadSize(32);
	radio.openReadingPipe(1,pipes[0]);
	radio.openWritingPipe(pipes[1]);
	radio.startListening();
	radio.printDetails(); //for Debugging
}

void loop() {

//Get temperature from sensor
float temperature =temp.getTemp();
// Assign temperature to payload, here am sending it as string
dtostrf(temperature,2,2,SendPayload);

//add a tag
strcat(SendPayload, "");   // add first string

//send a heartbeat
radio.stopListening();
bool ok = radio.write(&SendPayload,strlen(SendPayload));
radio.startListening(); 
Serial.println(SendPayload);  

// slow down a bit
Serial.print("waiting\n");
delay(1000); 
}


